﻿using System;
using Newtonsoft.Json;

namespace webWorkshop.Models
{
    public class Car
    {
        public int Id { get; set; }
        //[JsonProperty(PropertyName = "_id")]
        public string _id { get; set; }
        public string Name { get; set; }
        public int MaxSpeed { get; set; }
        public int Mileage { get; set; }
    }
}
