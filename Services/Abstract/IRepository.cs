﻿using System;
using System.Collections.Generic;

namespace webWorkshop.Services.Abstract
{
    public interface IRepository<T>
    {
        IEnumerable<T> GetAll();
        void Add(T item);
    }
}
