﻿using System;
using System.Collections.Generic;
using webWorkshop.Models;

namespace webWorkshop.Services.Abstract
{
    public interface IUserService
    {
        void CreateUser(User user);
        List<User> GetAllUsers();
    }
}
