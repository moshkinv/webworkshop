﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace webWorkshop.Services.Abstract
{
    public interface IAsyncRepository<T>
    {
        Task<IEnumerable<T>> GetAll();
        Task Add(T item);
        Task Update(T item);
        Task Delete(string id);
    }
}
