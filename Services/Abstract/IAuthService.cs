﻿using System;
using webWorkshop.Models;

namespace webWorkshop.Services.Abstract
{
    public interface IAuthService
    {
        bool ValidateUser(User user, out string token);
        bool ValidateToken(string token);
    }
}
