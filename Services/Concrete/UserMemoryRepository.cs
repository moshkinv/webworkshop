﻿using System;
using System.Collections.Generic;
using webWorkshop.Helpers;
using webWorkshop.Models;
using webWorkshop.Services.Abstract;

namespace webWorkshop.Services.Concrete
{
    public class UserMemoryRepository : IRepository<User>
    {
        private List<User> _users = new List<User>
        {
            new User
            {
                Username = "admin",
                Password = Sha1Helper.HashHash("admin" + Constants.Salt)
            }
        };

        public void Add(User item)
        {
            _users.Add(item);
        }

        public IEnumerable<User> GetAll()
        {
            return _users;
        }
    }
}
