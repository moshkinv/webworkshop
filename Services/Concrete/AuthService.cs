﻿using System;
using System.Linq;
using webWorkshop.Helpers;
using webWorkshop.Models;
using webWorkshop.Services.Abstract;

namespace webWorkshop.Services.Concrete
{
    public class AuthService : IAuthService
    {
        IUserService _userService;

        public AuthService(IUserService userService)
        {
            _userService = userService;
        }

        public bool ValidateToken(string token)
        {
            return _userService.GetAllUsers().Any(x => x.Password == token);
        }

        public bool ValidateUser(User user, out string token)
        {
            token = string.Empty;
            var hashedPassword = HashWithSalt(user.Password);

            if (_userService.GetAllUsers().Any(x =>
                x.Username == user.Username && x.Password == hashedPassword))
            {
                token = hashedPassword;
            }

            return token != string.Empty;
        }

        

        private string HashWithSalt(string source)
        {
            return Sha1Helper.HashHash(source + Constants.Salt);
        }
    }
}
