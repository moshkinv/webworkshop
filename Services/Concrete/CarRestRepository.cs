﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RestSharp;
using webWorkshop.Models;
using webWorkshop.Services.Abstract;

namespace webWorkshop.Services.Concrete
{
    public class CarRestRepository : IAsyncRepository<Car>
    {
        public CarRestRepository()
        {
        }

        public Task Add(Car item)
        {
            throw new NotImplementedException();
        }

        public async Task Delete(string id)
        {
            try
            {
                var client = new RestClient($"https://tbctest-d7a9.restdb.io/rest/cars/{id}");
                var request = new RestRequest(Method.DELETE);
                request.AddHeader("x-apikey", "6371523b60911d9468deebe0a547f1d191d04");
                var response = await client.ExecuteTaskAsync<string>(request);
            }
            catch (Exception ex)
            {

            }
        }

        public async Task<IEnumerable<Car>> GetAll()
        {
            List<Car> result = null;

            try
            {
                var client = new RestClient("https://tbctest-d7a9.restdb.io/rest/cars");
                var request = new RestRequest(Method.GET);
                request.AddHeader("x-apikey", "6371523b60911d9468deebe0a547f1d191d04");
                var response = await client.ExecuteTaskAsync<List<Car>>(request);

                result = response.Data;
            }
            catch (Exception ex)
            {

            }

            return result;
        }

        public Task Update(Car item)
        {
            throw new NotImplementedException();
        }
    }
}
