﻿using System;
using System.Collections.Generic;
using System.Linq;
using webWorkshop.Helpers;
using webWorkshop.Models;
using webWorkshop.Services.Abstract;

namespace webWorkshop.Services.Concrete
{
    public class UserService : IUserService
    {
        IRepository<User> _repository;

        public UserService(IRepository<User> repository)
        {
            _repository = repository;
        }

        public void CreateUser(User user)
        {
            user.Password = Sha1Helper.HashHash(user.Password + Constants.Salt);

            _repository.Add(user);
        }

        public List<User> GetAllUsers()
        {
            return _repository.GetAll().ToList();
        }
    }
}
