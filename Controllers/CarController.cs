﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using webWorkshop.Models;
using webWorkshop.Services.Abstract;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace webWorkshop.Controllers
{
    public class CarController : Controller
    {
        private readonly ILogger<CarController> _logger;
        IAsyncRepository<Car> _carRepo;

        public CarController(IAsyncRepository<Car> repo, ILogger<CarController> logger)
        {
            _carRepo = repo;
            _logger = logger;
        }

        // GET: /<controller>/
        public async Task<IActionResult> Index()
        {
            _logger.LogInformation("GET ALL THE CARS LIST!!11");
            var cars = await _carRepo.GetAll();

            return View(cars.ToList());
        }

        public async Task<IActionResult> Delete(string id)
        {
            await _carRepo.Delete(id);
            _logger.LogInformation($"DELETE CAR WITH ID={id}");

            return RedirectToAction("Index");
        }
    }
}
