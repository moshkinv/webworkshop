﻿using System;
using Microsoft.AspNetCore.Mvc;
using webWorkshop.Helpers;
using webWorkshop.Models;
using webWorkshop.Services.Abstract;

namespace webWorkshop.Controllers
{
    public class LoginController : Controller
    {
        IAuthService _authService;

        public LoginController(IAuthService authService)
        {
            _authService = authService;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Index(User user)
        {
            if (ModelState.IsValid)
            {
                if (_authService.ValidateUser(user, out var token))
                {
                    Response.Cookies.Append(Constants.TokenKey, token);

                    return Redirect(Url.Action("Index", "Home"));
                }
            }

            return View();
        }
    }
}
